#READ ME#

Hallo,
Op deze repo werk ik aan SLB en verbetering aan mijzelf en mijn studieloopbaanproject

Goede punten over mijzelf:

Ik ben enthousiast over wat ik doe en kan mijzelf erg goed presenteren tegenover een groep.
In teamverband weet ik goed en snel mijn rol te vinden en kan sturing en duidelijkheid aan een team geven.
Ik ben een doorzetter en laat lange dagen en ziekte mij niet tegenhouden om resultaat te boeken.
Ik denk in de toekomst, ik werk aan de hand van een planning en houdt mijn werk georganiseerd.

Projecten waar ik aan gewerkt heb:

https://bitbucket.org/wjschuiten/birdspeciesclassifier/src
In dit project heb ik gewerkt aan een machine learning alghoritme dat op basis van de lengte en breedte van botten, kon bepalen uit welk soort vogel deze kwamen.
Hierin heb ik meerdere alghoritmes getest met behulp van WEKA. Ik laat in het project zien dat ik snap hoe je een exploratory data analyse moet uitvoeren met R, en kan op basis van de resultaten kritisch beoordelen wat deze betekenen.
Uiteindelijk heb ik hiermee een optimaal alghoritme gekozen voor de dataset, en gereflecteerd over hoe het onderzoek beter uitgevoerd kan worden.

https://bitbucket.org/hmiddel/moleculegrapher/src
Dit project bevatte een opdracht van een docent in onderzoek. Het doel was een webapplicatie te maken die vanuit de output van een python script, moleculen dynamisch kan tekenen.
Voor dit project heb ik grotendeels de backend geschreven, de interactie tussen een java servlet en de achterliggende SQL database, java classes en het python script.




